
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';
var graf = [];

var username = "ois.seminar";
var password = "ois4fri";
var threeStooges = [{
    ime: "Dejan",
    priimek: "Bosnić",
    rojstvo: "1889-04-20",
    spol:'MALE',
    ehrId : null
  },{
    ime: "Vinko",
    priimek: "Sladoljev",
    rojstvo: "1944-06-06",
    spol:'MALE',
    ehrId: null
  },{
    ime: "Kapetanka",
    priimek: "Marvel",
    rojstvo: "2019-01-01",
    spol:'FEMALE',
    ehrId: null
}];

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();
  var spol = ($("#kreirajSpol").val() == "Moški") ? "MALE" : "FEMALE";

	if (!ime || !priimek || !datumRojstva || !spol || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0 || spol.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          gender: spol,
          additionalInfo: {
            "ehrId": ehrId,
          }
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}

function generirajPodatke(){
  for(var i =0;i<3;i++){
      generirajBolnika(i,function(){
        if(i==3){
          window.alert("Generirani pacienti :\n"+threeStooges[0].ehrId+"\n"+threeStooges[1].ehrId+"\n"+threeStooges[2].ehrId);
        }
      });
    }
  
   // window.alert("Generirani trije pacienti :\n"+threeStooges[0].ehrId+"\n"+threeStooges[1].ehrId+"\n"+threeStooges[2].ehrId);
}



function generirajBolnika(i,callback){
  var trenutni = threeStooges[i];
  
  var ime = trenutni.ime;
	var priimek = trenutni.priimek;
  var datumRojstva = trenutni.rojstvo;
  var spol = trenutni.spol;
  
  	$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          gender: spol,
          additionalInfo: {
            "ehrId": ehrId,
          }
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#preberiEHRid").val(ehrId);
              threeStooges[i].ehrId = ehrId;
              callback();
              add(threeStooges[i].ime+","+threeStooges[i].priimek+","+threeStooges[i].rojstvo+","+threeStooges[i].spol,
                (threeStooges[i].ime+" "+threeStooges[i].priimek),"preberiPredlogoBolnika");
              add(ehrId,(threeStooges[i].ime+" "+threeStooges[i].priimek),"preberiObstojeciVitalniZnak");
              add(ehrId,(threeStooges[i].ime+" "+threeStooges[i].priimek),"preberiObstojeciEHR");
              add(ehrId,(threeStooges[i].ime+" "+threeStooges[i].priimek),"preberiEhrIdZaVitalneZnake");
              nastaviVrednostiMeritev(i,threeStooges[i].ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
}

function add(ehrId, name, id) {
  var select = document.getElementById(id);
  var opt = document.createElement("option");
  opt.value = ehrId;
  opt.innerHTML = name;
  select.appendChild(opt);
}

/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
  		},
  		error: function(err) {
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
	}
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-success fade-in'>" +
          res.meta.href + ".</span>");
      },
      error: function(err) {
      	$("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura in telesna teža).
 */
function preberiMeritveVitalnihZnakov() {
 // console.log(pridobiMeritveTeze());
 // console.log(pridobiMeritveVisine());/////////////////
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
            "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
            " " + party.lastNames + "'</b>.</span><br/><br/>");
  				if (tip == "telesna temperatura") {
  					$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].temperature +
                      " " + res[i].unit + "</td></tr>";
                      graf.push(res[i].temperature)
  				        }
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				} else if (tip == "telesna teža") {
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].weight + " " 	+
                      res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}











$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
    $("#kreirajSpol").val(podatki[3]);
    console.log(podatki);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

  	$("#preberiMeritveVitalnihZnakov").on('click',function(){narisiGraf( pridobiMeritveTeze());});
});











function pridobiMeritveTeze() {
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
  var tabelaTemp = [];
  
	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevano identifikacijsko številko!");
      return tabelaTemp;
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
            "podatkov o meritvah bolnika <b>'" + party.firstNames +
            " " + party.lastNames + "'</b>.</span><br/><br/>");
  				
  					$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
  				        for (var i in res) {
                      tabelaTemp.push([parseInt(i),res[i].temperature]);
                  //    console.log(tabelaTemp);
  				        }
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
                    return tabelaTemp;
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
		return tabelaTemp;
	}
}

function pridobiMeritveVisine() {
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	//var tip = $("#preberiTipZaVitalneZnake").val();
  var tabelaVisin = [];
  
	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevano identifikacijsko številko!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
            "podatkov o meritvah bolnika <b>'" + party.firstNames +
            " " + party.lastNames + "'</b>.</span><br/><br/>");
  				
  					$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "height",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    
    				    	for (var i in res) {
    		            
                      tabelaVisin.push({
                                  time: res[i].time,
                                  visina: res[i].height/////////////////
                        })
  				        }
  				        
  				        
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
		return tabelaVisin;
	}
}

function nastaviVrednostiMeritev(iteracija, ehrId) {
  
  if (iteracija == 1) {
    
    for (var i = 10; i < 15; i++) {
      $("#dodajVitalnoEHR").val(ehrId);
      $("#dodajVitalnoDatumInUra").val('2019-05-' + i + 'T12:00');
    	$("#dodajVitalnoTelesnaVisina").val("185");
    	$("#dodajVitalnoTelesnaTeza").val(75 + Math.floor(Math.random()*3));
    	$("#dodajVitalnoTelesnaTemperatura").val(parseFloat(Math.random().toFixed(1)) + 36);
    
      dodajMeritveVitalnihZnakov();                 
    }
  } else if (iteracija == 2) {
    
    for (var i = 10; i < 15; i++) {
      $("#ehrId").val(ehrId);
      $("#datumInUra").val('2019-05-' + i + 'T12:00');
    	$("#telesnaVisina").val("164");
    	$("#telesnaTeza").val(65 + Math.floor(Math.random()*3));
    	$("#telesnaTemperatura").val(parseFloat(Math.random().toFixed(1)) + 38);
    
      dodajMeritveVitalnihZnakov();               
    }
  } else {
    
    for (var i = 10; i < 15; i++) {
        $("#ehrId").val(ehrId);
        $("#datumInUra").val('2019-05-' + i + 'T12:00');
      	$("#telesnaVisina").val("151");
      	$("#telesnaTeza").val(i < 14 ? (45 + Math.floor(Math.random()*3)) : (39 + Math.floor(Math.random()*3)));
      	$("#telesnaTemperatura").val(i < 14 ? (parseFloat(Math.random().toFixed(1)) + 36) : (39 + Math.floor(Math.random()*3)));
      
        dodajMeritveVitalnihZnakov(); 
    }
  }
}

function narisiGraf(tabela){
  var tabela = [[0,29],[1,38],[2,59]];
  console.log("tabela  :  "+tabela);
  // 2. Use the margin convention practice 
  var margin = {top: 50, right: 50, bottom: 50, left: 50}
    , width = (window.innerWidth)/2  - margin.left - margin.right// Use the window's width 
    , height = (window.innerHeight)/3 - margin.top - margin.bottom; // Use the window's height
  
  // The number of datapoints
  var n = (tabela.length>1) ? tabela.length : 1;
  console.log(n);
  // 5. X scale will use the index of our data
  var xScale = d3.scaleLinear()
      .domain([0, n-1]) // input
      .range([0, width]); // output
  
  // 6. Y scale will use the randomly generate number 
  var yScale = d3.scaleLinear()
      .domain([0, 200]) // input 
      .range([height, 0]); // output 
  
  // 7. d3's line generator
  var line = d3.line()
      .x(function(d, i) { return xScale(i); }) // set the x values for the line generator
      .y(function(d) { return yScale(d); }) // set the y values for the line generator 
      .curve(d3.curveMonotoneX) // apply smoothing to the line
  
  // 8. An array of objects of length N. Each object has key -> value pair, the key being "y" and the value is a random number
  //var dataset = d3.range(n).map(function(d) { return {"y": graf }; });
  
  // 1. Add the SVG to the page and employ #2
  var svg = d3.select("telo").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  
  // 3. Call the x axis in a group tag
  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(xScale)); // Create an axis component with d3.axisBottom
  
  // 4. Call the y axis in a group tag
  svg.append("g")
      .attr("class", "y axis")
      .call(d3.axisLeft(yScale)); // Create an axis component with d3.axisLeft
  
  // 9. Append the path, bind the data, and call the line generator 
  svg.append("path")
      .datum(tabela) // 10. Binds data to the line 
      .attr("class", "line") // Assign a class for styling 
      .attr("d", line); // 11. Calls the line generator 
      
      svg.append("path")
      .datum(tabela) // 10. Binds data to the line 
      .attr("class", "line") // Assign a class for styling 
      .attr("d", line); // 11. Calls the line generator 
      
      svg.append("path")
      .datum(tabela) // 10. Binds data to the line 
      .attr("class", "line") // Assign a class for styling 
      .attr("d", line); // 11. Calls the line generator 
  
  // 12. Appends a circle for each datapoint 
  svg.selectAll(".dot")
      .data(tabela)
    .enter().append("circle") // Uses the enter().append() method
      .attr("class", "dot") // Assign a class for styling
      .attr("cx", function(d, i) { return xScale(i) })
      .attr("cy", function(d) { return yScale(d.y) })
      .attr("r", 3)
      
}